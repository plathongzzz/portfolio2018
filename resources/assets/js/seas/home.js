if($('#sea-welcome').length > 0){
    var height = $( window ).height();
    $('#sea-welcome').height(height);
}

if($('#sea-contact').length > 0){
    var height = $( window ).height();
    $('#sea-contact').height(height);
}

$(".change-picture").click(function(){
    $("#sea-profile .mascot").toggleClass('active-me');
});

$( window ).resize(function() {
    var height = $( window ).height();
    $('#sea-welcome').height(height);
    $('#sea-contact').height(height);
});
