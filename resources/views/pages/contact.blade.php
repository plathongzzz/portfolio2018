<div id="contact">
    <div class="bg-aquar">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 wrap-center">
                    <div class="text-detail">
                        <h1>Contact</h1>
                        <p>See my contact for work</p>
                        <ul class="list-unstyled">
                            <li>Tritaya Thongvilard</li>
                            <li>live / Bangkok, Thailand</li>
                            <li>e-mail / plathongzzz@gmail.com</li>  
                        </ul>
                    </div>
                </div>
                <div class="col-xs col-sm-6 wrap-center">
                    <div class="treasure">
                        <img src="{{asset('images/contact/treasure.png')}}" alt="treasure" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="sand"></div>
        <div class="seaweed-1">
            <img src="{{asset('images/home/seaweed-1.png')}}" alt="seaweed" class="img-fluid  d-none d-sm-block">
        </div>
        <div class="fish-1">
            <img src="{{asset('images/home/fish.png')}}" alt="fish" class="img-fluid">
        </div>
        <div class="fish-2">
            <img src="{{asset('images/home/fish.png')}}" alt="fish" class="img-fluid d-none d-lg-block">
        </div>
    </div>
</div>

@section('js')

@endsection