<div id="about">
    <div class="bg-aquar">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-detail">
                        <h1>About me</h1>
                        <p>"I don't know what I like or I love But I will try to find it."</p>
                        <p>I know what I'm good at</p>
                    </div>
                </div>
            </div>
            <div class="fish">
                <img src="{{asset('images/about/about-what-i-do.png')}}" alt="what-i-do" class="img-fluid d-none d-sm-block">
                <img src="{{asset('images/about/about-what-i-do-mobile.png')}}" alt="what-i-do" class="img-fluid d-block d-sm-none">
            </div>
        </div>
    </div>
</div>

@section('js')

@endsection