<div id="work">
    <div class="container">
        <h1>My Work</h1>
    </div>
    <div class="bg-wave"></div>
    <div class="bg-aquar">
        <div class="container">
            <div id="carouselWorkIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselWorkIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselWorkIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselWorkIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselWorkIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselWorkIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselWorkIndicators" data-slide-to="5"></li>
                    <li data-target="#carouselWorkIndicators" data-slide-to="6"></li>
                    <li data-target="#carouselWorkIndicators" data-slide-to="7"></li>
                    <li data-target="#carouselWorkIndicators" data-slide-to="8"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="{{asset('images/work/cpmeiji.jpg')}}" alt="cpmeiji">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>Meiji.com</h5>
                                <p>
                                    Develop HTML CSS and Script <br>
                                    link : <a href="https://cpmeiji.com/" target="_blank">Meiji.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/work/BulgariaNew.jpg')}}" alt="BulgariaNew">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>Meiji - Camapgin Bulgaria New</h5>
                                <p>
                                    Develop HTML CSS and Script <br>
                                    link : <a href="https://cpmeiji.com/BulgariaNew/iphoneX" target="_blank">Camapgin Bulgaria New</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/work/bulgaria-new-organicgain.jpg')}}" alt="bulgaria-new-organicgain">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>Meiji - Camapgin bulgaria new organicgain</h5>
                                <p>
                                    Develop HTML CSS and Script <br>
                                    link : <a href="https://cpmeiji.com/bulgaria-new-organicgain" target="_blank">Camapgin bulgaria new organicgain</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/work/sharekitchen.jpg')}}" alt="yourgrade">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>Share kitchen - Personal project</h5>
                                <p>
                                    Design , Develop HTML CSS , Script and Backend<br>
                                    link : <a href="http://plathongz.in.th/senior_project/design/" target="_blank">Share kitchen </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/work/yourgrade.jpg')}}" alt="yourgrade">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>Yourgrade - Personal project</h5>
                                <p>
                                    Design , Develop HTML CSS and Script <br>
                                    link : <a href="http://plathongz.in.th/yourgrade/" target="_blank">Your grade</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/work/chompla.jpg')}}" alt="chompla">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>Chompla - Personal project</h5>
                                <p>
                                    Design , Develop HTML CSS and Script <br>
                                    link : <a href="http://plathongz.in.th/chompla/" target="_blank">Chompla</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/work/lookbook.jpg')}}" alt="lookbook">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>Lookbook - Personal project</h5>
                                <p>
                                    Develop HTML CSS , Script and Backend<br>
                                    link : <a href="http://plathongz.in.th/lookbook/" target="_blank">Lookbook</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/work/jpv.jpg')}}" alt="jpv">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>Lookbook - Personal project</h5>
                                <p>
                                    Design , Develop HTML CSS and Script<br>
                                    link : <a href="http://plathongz.in.th/58th-jpv/" target="_blank">jpv</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('images/work/YWC.jpg')}}" alt="ywc">
                        <div class="carousel-caption d-none d-md-block">
                            <div class="bg-caption">
                                <h5>YWC - Personal project</h5>
                                <p>
                                    Design , Develop HTML CSS and Script<br>
                                    link : <a href="http://plathongz.in.th/ywc/" target="_blank">YWC</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselWorkIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselWorkIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>

@section('js')

@endsection