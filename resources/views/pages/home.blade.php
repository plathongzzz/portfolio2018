<div id="home">
    <div class="bg-aquar">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 wrap-center">
                    <div class="text-detail">
                        <h1>Hello! Welcome to my aquarium</h1>
                        <p>"I hope you would understand me better, you go to <a href="#about">my profile</a>
                        If you want to see my works go to this <a href="#work">works</a>
                        In the end you want to see my contact go to this <a href="#contact">contact</a>"</p>
                    </div>
                </div>
                <div class="col-xs col-sm-6 wrap-center">
                    <div class="mascot">
                        <img src="{{asset('images/home/mascot.png')}}" alt="plathong hello world" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="sand"></div>
        <div class="seaweed">
            <img src="{{asset('images/home/seaweed.png')}}" alt="seaweed" class="img-fluid">
        </div>
        <div class="seaweed-1">
            <img src="{{asset('images/home/seaweed-1.png')}}" alt="seaweed" class="img-fluid">
        </div>
        <div class="fish-1">
            <img src="{{asset('images/home/fish.png')}}" alt="fish" class="img-fluid">
        </div>
        <div class="fish-2">
            <img src="{{asset('images/home/fish.png')}}" alt="fish" class="img-fluid">
        </div>
    </div>
</div>

@section('js')

@endsection