<div id="sea-welcome">
    <div class="bg-sky">
        <div class="cloude cloude-1">
            <img src="{{asset('images/seas/welcome/cloude-1.png')}}" alt="cloude" class="img-fluid">
        </div>
        <div class="cloude cloude-2">
            <img src="{{asset('images/seas/welcome/cloude-2.png')}}" alt="cloude" class="img-fluid">
        </div>
        <div class="cloude cloude-3">
            <img src="{{asset('images/seas/welcome/cloude-3.png')}}" alt="cloude" class="img-fluid">
        </div>
        <div class="bird bird-1">
            <img src="{{asset('images/seas/welcome/bird-1.png')}}" alt="bird" class="img-fluid">
        </div>
        <div class="bird bird-2">
            <img src="{{asset('images/seas/welcome/bird-2.png')}}" alt="bird" class="img-fluid">
        </div>
        <div class="bird bird-3">
            <img src="{{asset('images/seas/welcome/bird-3.png')}}" alt="bird" class="img-fluid">
        </div>
        <div class="text-title">
            <div class="position-relative">
                <h1>
                    welcome <br> to
                </h1>
                <div class="see-more">
                    <a href="#sea-home"><i class="animated infinite bounce delay-2s fas fa-chevron-down"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

@section('js')
@endsection