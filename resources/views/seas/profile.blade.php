<div id="sea-profile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="group-data">
                    <h1>EDUCATION</h1>
                    <b>Chanpradittharam Wittayakom School (2009 - 2011)</b>
                    <p><span>Major :</span>  Science-Mathematics</p>
                    <b>Silpakorn University  (2012 - 2015)</b>
                    <p><span>faculty :</span>  Information and Communication Technology</p>
                    <p><span>Major :</span>  Web & Interactive Design</p>
                </div>
                <div class="group-data">
                    <h1>EXPERIENCE</h1>
                    <b>Company</b>
                    <p>Programmer (frontend / backend) at PRANEAT Co., Ltd.</p>
                    <b>Internship</b>
                    <p>Internship position is a UX designer at Dek-D Interactive Co.,ltd.</p>
                    <b>Teacher assistant</b>
                    <p>Teacher assistant in subject  C++ and Pre poduction at Silpakorn University </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="blog-change-picture">
                    <a class="change-picture"><i class="animated infinite tada delay-2s far fa-images"></i></a>
                </div>
                <div class="mascot">
                    <img src="{{asset('images/seas/profile/mermaid-2.png')}}" alt="mermaid" class="img-fluid mermaid">
                    <img src="{{asset('images/seas/profile/me.png')}}" alt="me" class="img-fluid me">
                </div>
            </div>
        </div>
    </div>
</div>