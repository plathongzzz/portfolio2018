<div id="sea-work">
    <div class="container">
        <div class="label">
            <h1>My work <span></span></h1>
            <img src="{{asset('images/seas/work/label.png')}}" alt="label" class="img-fluid">
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="wrap-shell">
                    <div class="shell">
                        <a data-toggle="modal" data-target="#workModal">
                            <span>2018</span>
                            <img src="{{asset('images/seas/work/shell-1.png')}}" alt="work 2018" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="wrap-shell">
                    <div class="shell">
                        <a href="http://plathongz.in.th/portfolio/2014/" target="_blank">
                            <span>2014</span>
                            <img src="{{asset('images/seas/work/shell-1.png')}}" alt="work 2014" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="seaweed seaweed-1">
        <img src="{{asset('images/seas/work/seaweed-1.png')}}" alt="seaweed" class="img-fluid">
    </div>
    <div class="seaweed seaweed-2">
        <img src="{{asset('images/seas/work/seaweed-2.png')}}" alt="seaweed" class="img-fluid">
    </div>

    <!-- Modal -->
<div class="modal fade" id="workModal" tabindex="-1" role="dialog" aria-labelledby="workModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="workModalLabel">My Work</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div id="carouselWorkIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselWorkIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselWorkIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselWorkIndicators" data-slide-to="2"></li>
                            <li data-target="#carouselWorkIndicators" data-slide-to="3"></li>
                            <li data-target="#carouselWorkIndicators" data-slide-to="4"></li>
                            <li data-target="#carouselWorkIndicators" data-slide-to="5"></li>
                            <li data-target="#carouselWorkIndicators" data-slide-to="6"></li>
                            <li data-target="#carouselWorkIndicators" data-slide-to="7"></li>
                            <li data-target="#carouselWorkIndicators" data-slide-to="8"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="{{asset('images/work/cpmeiji.jpg')}}" alt="cpmeiji">
                                <div class="bg-caption">
                                    <h5>Meiji.com</h5>
                                    <p>
                                        Develop HTML CSS and Script <br>
                                        link : <a href="https://cpmeiji.com/" target="_blank">Meiji.com</a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset('images/work/BulgariaNew.jpg')}}" alt="BulgariaNew">
                                <div class="bg-caption">
                                    <h5>Meiji - Camapgin Bulgaria New</h5>
                                    <p>
                                        Develop HTML CSS and Script <br>
                                        link : <a href="https://cpmeiji.com/BulgariaNew/iphoneX" target="_blank">Camapgin Bulgaria New</a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset('images/work/bulgaria-new-organicgain.jpg')}}" alt="bulgaria-new-organicgain">
                                <div class="bg-caption">
                                    <h5>Meiji - Camapgin bulgaria new organicgain</h5>
                                    <p>
                                        Develop HTML CSS and Script <br>
                                        link : <a href="https://cpmeiji.com/bulgaria-new-organicgain" target="_blank">Camapgin bulgaria new organicgain</a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset('images/work/sharekitchen.jpg')}}" alt="yourgrade">
                                <div class="bg-caption">
                                    <h5>Share kitchen - Personal project</h5>
                                    <p>
                                        Design , Develop HTML CSS , Script and Backend<br>
                                        link : <a href="http://plathongz.in.th/senior_project/design/" target="_blank">Share kitchen </a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset('images/work/yourgrade.jpg')}}" alt="yourgrade">
                                <div class="bg-caption">
                                    <h5>Yourgrade - Personal project</h5>
                                    <p>
                                        Design , Develop HTML CSS and Script <br>
                                        link : <a href="http://plathongz.in.th/yourgrade/" target="_blank">Your grade</a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset('images/work/chompla.jpg')}}" alt="chompla">
                                <div class="bg-caption">
                                    <h5>Chompla - Personal project</h5>
                                    <p>
                                        Design , Develop HTML CSS and Script <br>
                                        link : <a href="http://plathongz.in.th/chompla/" target="_blank">Chompla</a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset('images/work/lookbook.jpg')}}" alt="lookbook">
                                <div class="bg-caption">
                                    <h5>Lookbook - Personal project</h5>
                                    <p>
                                        Develop HTML CSS , Script and Backend<br>
                                        link : <a href="http://plathongz.in.th/lookbook/" target="_blank">Lookbook</a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset('images/work/jpv.jpg')}}" alt="jpv">
                                <div class="bg-caption">
                                    <h5>Lookbook - Personal project</h5>
                                    <p>
                                        Design , Develop HTML CSS and Script<br>
                                        link : <a href="http://plathongz.in.th/58th-jpv/" target="_blank">jpv</a>
                                    </p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{asset('images/work/YWC.jpg')}}" alt="ywc">
                                <div class="bg-caption">
                                    <h5>YWC - Personal project</h5>
                                    <p>
                                        Design , Develop HTML CSS and Script<br>
                                        link : <a href="http://plathongz.in.th/ywc/" target="_blank">YWC</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselWorkIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselWorkIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</div>


@section('js')
@endsection