<div id="sea-skill">
    <div class="container">
        <div class="text-detail">
            <p>"I am motivated and enthusiastic by new challenges and tasks and take excellent approach to achieve success in all projects. I like to work in a complex projects which have scope for learning and challenge."</p>
            <p>I know what I'm good at</p>
        </div>
        <div class="wrap-turtle">
            <div class="turtle turtle-1">
                <h1>HTML</h1>
                <img src="{{asset('images/seas/skill/turtle.png')}}" alt="HTML" class="img-fluid">
            </div>
            <div class="turtle turtle-2">
                <h1>CSS</h1>
                <img src="{{asset('images/seas/skill/turtle.png')}}" alt="CSS" class="img-fluid">
            </div>
            <div class="turtle turtle-3">
                <h1>SQL</h1>
                <img src="{{asset('images/seas/skill/turtle.png')}}" alt="SQL" class="img-fluid">
            </div>
            <div class="turtle turtle-4">
                <h1>JQuery</h1>
                <img src="{{asset('images/seas/skill/turtle.png')}}" alt="JQuery" class="img-fluid">
            </div>
            <div class="turtle turtle-5">
                <h1>Javascript</h1>
                <img src="{{asset('images/seas/skill/turtle.png')}}" alt="Javascript" class="img-fluid">
            </div>
            <div class="turtle turtle-6">
                <h1>PHP</h1>
                <img src="{{asset('images/seas/skill/turtle.png')}}" alt="PHP" class="img-fluid">
            </div>
        </div>
    </div>
</div>

@section('js')
@endsection