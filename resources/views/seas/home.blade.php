<div id="sea-home">
    <div class="text-detail">
        <h1>Hello! Welcome to my sea</h1>
        <p>"I hope you would understand me better, you go to <a href="#sea-profile">my profile</a>
        If you want to see my works go to this <a href="#sea-work">works</a>
        In the end you want to see my contact go to this <a href="#sea-contact">contact</a>"</p>
    </div> 
    <div class="mascot">
        <img src="{{asset('images/seas/home/mermaid.png')}}" alt="bird" class="img-fluid">
    </div>
    <div class="water water-1">
        <img src="{{asset('images/seas/home/wave-1.png')}}" alt="bird" class="img-fluid">
    </div>
    <div class="water water-2">
        <img src="{{asset('images/seas/home/wave-2.png')}}" alt="bird" class="img-fluid">
    </div>
</div>

@section('js')
@endsection