<div id="sea-contact">
    <div class="container">
        <div class="label">
            <h1>Contact</h1>
            <p>Tritaya Thongvilard (Phing)</p>
            <p>live / Bangkok, Thailand</p>
            <p>e-mail / plathongzzz@gmail.com</p>
        </div>
    </div>
    <div class="bubble bubble-1">
        <img src="{{asset('images/seas/contact/bubble-1.png')}}" alt="bubble" class="img-fluid">
    </div>
    <div class="bubble bubble-2">
        <img src="{{asset('images/seas/contact/bubble-2.png')}}" alt="bubble" class="img-fluid">
    </div>
    <div class="bubble bubble-3">
        <img src="{{asset('images/seas/contact/bubble-3.png')}}" alt="bubble" class="img-fluid">
    </div>
    <div class="bubble bubble-4">
        <img src="{{asset('images/seas/contact/bubble-4.png')}}" alt="bubble" class="img-fluid">
    </div>
    <div class="bubble bubble-5">
        <img src="{{asset('images/seas/contact/bubble-1.png')}}" alt="bubble" class="img-fluid">
    </div>
    <div class="bubble bubble-6">
        <img src="{{asset('images/seas/contact/bubble-2.png')}}" alt="bubble" class="img-fluid">
    </div>
</div>

@section('js')
@endsection