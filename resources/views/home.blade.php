@extends('layouts.master')
@section('content')
    @include('pages.home')
    @include('pages.about')
    @include('pages.work')
    @include('pages.contact')
@endsection