@extends('layouts.master')
@section('content')
    @include('seas.welcome')
    @include('seas.home')
    @include('seas.profile')
    @include('seas.skill')
    @include('seas.work')
    @include('seas.contact')

@endsection